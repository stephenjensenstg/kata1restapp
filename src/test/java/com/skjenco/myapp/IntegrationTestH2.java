package com.skjenco.myapp;


import com.skjenco.myapp.service.UserService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class IntegrationTestH2 {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    private EmbeddedDatabase db;

    @Autowired
    private UserService userService;


    @Before
    public void setup() throws Exception {


        db = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("db/h2.sql")
                .build();
        jdbcTemplate.setDataSource(db);
    }

    @After
    public void teardown() throws Exception {

        if(db!=null){
            db.shutdown();
        }

    }


    @Test
    public void myIntegrationTest() throws Exception {

            userService.getUserDescription();


    }



}
