package com.skjenco.myapp;

import com.skjenco.myapp.service.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class MyappApplicationTests {

	@Autowired
	UserService userService;

	@Test
	public void contextLoads() {
		Assert.assertEquals("stephen",userService.getUserDescription());

	}

}
