package com.skjenco.myapp.repository;

import com.skjenco.myapp.model.UserInfo;
import org.springframework.data.repository.CrudRepository;

public interface UserInfoRepository extends CrudRepository<UserInfo, Long> {

    UserInfo getUserInfoByUserId(Long id);
}
