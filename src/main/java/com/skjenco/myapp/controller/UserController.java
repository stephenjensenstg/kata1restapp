package com.skjenco.myapp.controller;

import com.skjenco.myapp.model.User;
import com.skjenco.myapp.model.UserInfo;
import com.skjenco.myapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private UserService userService;


    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    User getUser(@PathVariable("id") Long id){
        return userService.getUser(id);
    }


    @RequestMapping(value = "/userInfo/{userId}", method = RequestMethod.GET)
    UserInfo getUserInfo(@PathVariable("userId") Long id){
        return userService.getUserInfo(id);
    }

}
