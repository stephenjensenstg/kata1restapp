package com.skjenco.myapp.model;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="user")
public class User {

    //TODO create a User

    @Id
    @Column(name = "id")
    Long id;


    @Column(name = "user_name")
    String userName;

    @Column(name = "description")
    String description;

//This goes beyond the scope of ;the kata
//    @OneToMany(mappedBy = "user", cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
//    List<UserInfo> userInfos;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
