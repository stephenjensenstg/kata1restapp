package com.skjenco.myapp.service;

import com.skjenco.myapp.model.User;
import com.skjenco.myapp.model.UserInfo;

public interface UserService {

    String getUserDescription();

    User getUser(Long id);

    UserInfo getUserInfo(Long id);

}
