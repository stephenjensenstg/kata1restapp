package com.skjenco.myapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * My Kata first time  Jan 6 2018  Time 3 hour 54 minutes
 *
 *
 * Requirements
 * - Spring boot Web app.
 - JPA/Hibernate
 - Database
 - In memory
 - Another Database Oracle, Mysql, Postgress etcable
 - Tables
 - User
 - id  PK
 - user_name  (Unique index)
 - Description
 - Address
 - id    PK
 - street
 - city
 - state
 - zip
 - user_info
 - id    PK
 - user_id    FK
 - address_id   FK
 - First
 - Last
 - Description
 - Must have model level referential integrity.
 - Controller, Service and Repository Layer

 - Unit Test
 - Integration Test
 - Rest API
 *
 *
 * Critique
 *  Just done not good unit test or integeration or really did much of anything other then
 *  prove it working.
 *
 *
 */



@SpringBootApplication
public class MyappApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyappApplication.class, args);
	}
}
